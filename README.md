# Wardrobify

Team:

* Yosef Dandis - Shoes
* wilkin ruiz - hats


## Design

## Shoes microservice

Explain your models and integration with the wardrobe
microservice, here.

## Hats microservice

I designed a model for a hat object that includes properties such as fabric, style name, hat color (implemented using the CharField class), hat URL (implemented using a URLField), and location (implemented using a foreign key). Additionally, I created a Location value object model which interacts with the wardrobe microservice through a poller to facilitate the integration of the hat object into the system.
