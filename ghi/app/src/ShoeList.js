import React from "react"

class ShoeList extends React.Component{
    constructor(props) {
        super(props)
        this.state = {shoes: []}
    }

    async componentDidMount() {
        const shoesUrl = "http://localhost:8080/api/shoes";
        const shoeResponse = await fetch(shoesUrl);
        if (shoeResponse.ok) {
            const shoesJSON = await shoeResponse.json()
            this.setState({shoes: shoesJSON.shoes})
        }
    }

    async delete(id) {
        const deleteShoeUrl = `http://localhost:8080/api/shoes/${id}`
        const fetchConfig = {
            method: "delete",
        }
        const deleteResponse = await fetch(deleteShoeUrl, fetchConfig);
        if (deleteResponse.ok){
            window.location.reload(false)
        }
        

    }


    render(){
    return (
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>
                        <a href="http://localhost:3000/shoes/new" role="button" >Create New Shoe</a>
                    </th>
                </tr>
                <tr>
                    <th>Manufacturer</th>
                    <th>Model Name</th>
                    <th>Color</th>
                    <th>Bin</th>
                    <th>Photo</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                {this.state.shoes.map(shoe => (
                    <tr key={shoe.id}>
                        <td>{shoe.manufacturer}</td>
                        <td>{shoe.model_name}</td>
                        <td>{shoe.color}</td>
                        <td>{shoe.bin.closet_name}</td>
                        <td>
                            <img className="photo" src={shoe.picture_url} />
                        </td>
                        <td><button onClick={()=>this.delete(shoe.id)} >Delete</button></td>
                    </tr>
                ))}
            </tbody>
        </table>
    )
}
}

export default ShoeList