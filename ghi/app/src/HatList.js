import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";

const deleteHat = async (id) => {
  fetch(`http://localhost:8090/api/hats/${id}/`, {
    method: "DELETE",
    headers: {
      "Content-Type": "application/json",
    },
  });
  window.location.reload();
};

function HatColumn(props) {
  return (
    <div className="col">
      {props.list.map((data) => {
        const hat = data;
        return (
          <div key={hat.id}>
            <div className="card mb-3 shadow">
              <img src={hat.hat_url} className="card-img-top" />

              <div className="card-body">
                <h5 className="card-title">
                  {hat.style_name} {hat.fabric}
                </h5>
                <h6 className="card-subtitle mb-2 text-muted">
                  {hat.location.section_number}
                </h6>
                <h6 className="card-subtitle mb-2 text-muted">
                  color: {hat.hat_color}
                </h6>
              </div>
              <div className="card-footer">
                <button onClick={() => deleteHat(hat.id)} type="button">
                  Delete
                </button>
              </div>
            </div>
          </div>
        );
      })}
    </div>
  );
}

const HatsList = () => {
  const [hatColumns, setHatColumns] = useState([[], [], []]);

  const fetchData = async () => {
    const url = "http://localhost:8090/api/hats/";

    try {
      const response = await fetch(url);
      if (response.ok) {
        const data = await response.json();

        const requests = [];
        for (let hat of data.hats) {
          const detailUrl = `http://localhost:8090/api/hats/${hat.id}`;
          requests.push(fetch(detailUrl));
        }

        const responses = await Promise.all(requests);

        const columns = [[], [], []];

        let i = 0;
        for (const hatResponse of responses) {
          if (hatResponse.ok) {
            const details = await hatResponse.json();
            columns[i].push(details);
            i = i + 1;
            if (i > 2) {
              i = 0;
            }
          } else {
            console.error(hatResponse);
          }
        }

        setHatColumns(columns);
      }
    } catch (e) {
      console.error(e);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <>
      <div className="px-4 py-5 my-5 mt-0 text-center ">
        <img
          className="bg-white rounded shadow d-block mx-auto mb-4"
          src="/logo.svg"
          alt=""
          width="600"
        />
        <h1 className="display-5 fw-bold">Wardrobe HATS!</h1>
        <div className="col-lg-6 mx-auto">
          <p className="lead mb-4">
            This is your one-stop shop for everything related to keeping track
            of your Hats
          </p>
          <div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
            <Link
              to="/hats/new"
              className="btn btn-primary btn-lg px-4 gap-3"
              style={{
                transform: "translate3d(0, 0, 0)",
                transition: "transform 0.3s ease-in-out",
              }}
              onMouseEnter={(e) =>
                (e.target.style.transform = "translate3d(0, -5px, 0)")
              }
              onMouseLeave={(e) =>
                (e.target.style.transform = "translate3d(0, 0, 0)")
              }
            >
              Create a hat!
            </Link>
          </div>
        </div>
      </div>
      <div className="container">
        <h2>List of Hats</h2>
        <div className="row">
          {hatColumns.map((hatList, index) => {
            return <HatColumn key={index} list={hatList} />;
          })}
        </div>
      </div>
    </>
  );
};

export default HatsList;
