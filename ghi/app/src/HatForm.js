import React, { useEffect, useState } from "react";

function HatForm(props) {
  const [locations, setLocations] = useState([]);
  const [fabric, setFabric] = useState("");
  const [styleName, setStyleName] = useState("");
  const [hatColor, setHatColor] = useState("");
  const [hatUrl, setHatUrl] = useState("");
  const [location, setLocation] = useState("");

  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = {};
    data.style_name = styleName;
    data.hat_color = hatColor;
    data.hat_url = hatUrl;
    data.fabric = fabric;
    data.location = location;
    delete data.styleName;
    delete data.hatColor;
    delete data.hatUrl;
    delete data.locations;
    delete data.fabric;
    console.log(data);

    const HatsUrl = "http: //localhost:8090/api/hats/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(HatsUrl, fetchConfig);
    if (response.ok) {
      const newHat = await response.JSON();
      console.log(newHat);

      setStyleName("");
      setHatColor("");
      setHatUrl("");
      setFabric("");
      setLocation("");
    }
  };

  const handleFabricChange = (event) => {
    const value = event.target.value;
    setFabric(value);
  };

  const handleStyleChange = (event) => {
    const value = event.target.value;
    setStyleName(value);
  };

  const handleColorChange = (event) => {
    const value = event.target.value;
    setHatColor(value);
  };

  const handleUrlChange = (event) => {
    const value = event.target.value;
    setHatUrl(value);
  };

  const handleLocationChange = (event) => {
    const value = event.target.value;
    setLocation(value);
  };

  const fetchData = async () => {
    const url = "http://localhost:8100/api/locations/";
    const response = await fetch(url);

    if (response.ok) {
      const newData = await response.json();
      setLocations(newData.locations);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);
  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a new Hat</h1>
          <form onSubmit={handleSubmit} id="create-hat-form">
            <div className="form-floating mb-3">
              <input
                onChange={handleFabricChange}
                value={fabric}
                placeholder="fabric"
                required
                type="text"
                name="fabric"
                id="fabric"
                className="form-control"
              />
              <label htmlFor="fabric">Fabric</label>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={handleStyleChange}
                value={styleName}
                placeholder="Style Name"
                required
                type="text"
                name="styleName"
                id="styleName"
                className="form-control"
              />
              <label htmlFor="styleName">Style Name</label>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={handleColorChange}
                value={hatColor}
                placeholder="Color"
                required
                type="text"
                name="hatColor"
                id="hatColor"
                className="form-control"
              />
              <label htmlFor="hatColor">Color</label>
            </div>
            <div className="mb-3">
              <label htmlFor="hatUrl" className="form-label">
                Picture Url
              </label>
              <input
                onChange={handleUrlChange}
                value={hatUrl}
                name="hatUrl"
                required
                type="url"
                id="hatUrl"
                className="form-control"
                rows="3"
              ></input>
            </div>
            <div className="mb-3">
              <select
                onChange={handleLocationChange}
                value={location}
                required
                id="location"
                name="location"
                className="form-select"
              >
                <option value="">Choose a location</option>
                {locations.map((location) => {
                  return (
                    <option key={location.id} value={location.href}>
                      {location.closet_name}
                    </option>
                  );
                })}
              </select>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default HatForm;
