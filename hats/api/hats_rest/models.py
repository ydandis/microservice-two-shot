from django.db import models




class LocationVO(models.Model):
    import_href = models.CharField(max_length=200, blank=True, null=True, unique=True)
    closet_name = models.CharField(max_length=100)


    def __str__(self):
        return f"{self.closet_name}"


class Hat(models.Model):
    fabric = models.CharField(max_length=50)
    style_name = models.CharField(max_length=120)
    hat_color = models.CharField(max_length=50)
    hat_url = models.URLField()
    location = models.ForeignKey(
        LocationVO,
        related_name= "hat",
        on_delete=models.CASCADE,
    )
