from django.http import JsonResponse
# from django.shortcuts import render
from django.views.decorators.http import require_http_methods
import json

from .models import Hat, LocationVO
from common.json import ModelEncoder




class LocationVODetailEncoder(ModelEncoder):
    model = LocationVO
    properties = [
        "import_href",
        "closet_name",

    ]


class HatsListEncoder(ModelEncoder):
    model = Hat
    properties = [
        "id",
        "fabric",
        "style_name",
        "hat_color",
        "hat_url",
        "location",
    ]
    encoders = {"location": LocationVODetailEncoder()}





@require_http_methods(["GET", "POST"])
def list_of_hats(request):
    if request.method == "GET":
        hats = Hat.objects.all()
        return JsonResponse(
            {"hats": hats},
            encoder=HatsListEncoder,
            safe=False,
        )


    else:
        content = json.loads(request.body)

        try:
            href = content["location"]
            location = LocationVO.objects.get(import_href= href)
            content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400,
            )

        hats = Hat.objects.create(**content)
        return JsonResponse(
            hats,
            encoder=HatsListEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "GET"])
def detail_of_hats(request, pk):
    if request.method == "GET":
        hat = Hat.objects.get(id=pk)
        return JsonResponse(
            hat,
            encoder=HatsListEncoder,
            safe=False,
        )
    else:
        count, _ = Hat.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
