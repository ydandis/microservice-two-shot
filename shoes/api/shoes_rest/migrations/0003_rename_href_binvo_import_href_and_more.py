# Generated by Django 4.0.3 on 2023-04-20 09:01

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('shoes_rest', '0002_binvo_remove_shoe_name_shoe_bin'),
    ]

    operations = [
        migrations.RenameField(
            model_name='binvo',
            old_name='href',
            new_name='import_href',
        ),
        migrations.RemoveField(
            model_name='binvo',
            name='bin_number',
        ),
        migrations.RemoveField(
            model_name='binvo',
            name='bin_size',
        ),
    ]
